import datetime
import re
from pathlib import Path

import pytest
import test_constants as TEST_CST
from hypothesis import given
from hypothesis import strategies as st
from test_utils import ContextLock

import calimag.errors as err
from calimag.config import (
    GetConfig,
    check_empty_fields,
    check_mandatory_fields,
    check_path_fields,
)

CONFIG_FILEPATH = TEST_CST.FILEPATHS["v2019_07-2p-Max"]["config"]


def test_GetConfig():
    with CONFIG_FILEPATH.open("r") as fid:
        file_content = fid.read()

    # Remove timezone info
    nwbfile_content = """
[NWBFile]
    session_description_mandatory = "My awesome session"
    lab = "Fleischmann Lab"
    institution = "Brown University"
"""
    pattern = re.compile(r"(?sm)(\[NWBFile\])(.*?)(^\s*$)")
    file_content_new = pattern.sub(nwbfile_content, file_content)
    assert file_content_new != file_content
    config_filepath = Path("test_config.toml")
    with ContextLock(file_name=config_filepath, file_content=file_content_new):
        config = GetConfig(config_filepath=config_filepath)

        DataInput = config.get("DataInput")
        for filetype, path in DataInput.items():
            if "path" in (f := filetype.lower()) or "directory" in f:
                assert isinstance(path, Path)
            else:
                assert isinstance(path, str)
        Subject = config.get("Subject")
        for key, item in Subject.items():
            if key == "date_of_birth":
                isinstance(item, datetime.datetime)
            else:
                isinstance(item, str)
        NWBFile = config.get("NWBFile")
        assert NWBFile["identifier"] == "ace47c20624973e1ac0215782504d770"
        assert (
            NWBFile["session_start_time"].isoformat()
            == "2020-02-04T18:01:38.714876-05:00"
        )
        GetConfig.cache_clear()


@pytest.mark.parametrize(
    "subject_items, expected_items",
    [
        (
            ["", "Mouse #8", "", "male", "mouse", 8, 31, "1979-05-27"],
            ["1979-05-27T00:00:00-05:00"],
        ),
        (
            ["", "Mouse #8", "", "male", "mouse", 8, 31, ""],
            ["1979-05-27T00:00:00-05:00"],
        ),
        (
            [
                "73w (ended)",
                "Mouse #8",
                "",
                "male",
                "mouse",
                8,
                31,
                "1979-05-27T07:32:00",
            ],
            ["1979-05-27T07:32:00-05:00"],
        ),
        (
            [
                "73w (ended)",
                "Mouse #8",
                "tdTomato +/-",
                "",
                "mouse",
                8,
                "",
                "1979-05-27T00:32:00",
            ],
            ["1979-05-27T00:32:00-05:00"],
        ),
        (
            [
                "73w (ended)",
                "Mouse #8",
                "tdTomato +/-",
                "male",
                "",
                "",
                31,
                "1979-05-27T00:32:00.999999",
            ],
            ["1979-05-27T00:32:00.999999-05:00"],
        ),
        ([], []),
    ],
)
def test_GetConfig_check_Subject(subject_items, expected_items):
    with CONFIG_FILEPATH.open("r") as fid:
        file_content = fid.read()

    # Replace Subject section with dynamic content
    if subject_items:
        subject_content = f"""
[Subject]
    age = "{subject_items[0]}"
    description = "{subject_items[1]}"
    genotype = "{subject_items[2]}"
    sex = "{subject_items[3]}"
    species = "{subject_items[4]}"
    subject_id = {subject_items[5] if subject_items[5] else "''"}
    weight = {subject_items[6] if subject_items[6] else "''"} # [grams]
    date_of_birth = {subject_items[7] if subject_items[7] else "''"}
"""
    else:
        subject_content = ""
    pattern = re.compile(r"(?sm)(\[Subject\])(.*?)(^\s*$)")
    file_content_new = pattern.sub(subject_content, file_content)
    assert file_content_new != file_content

    config_filepath = Path("test_config.toml")
    with ContextLock(file_name=config_filepath, file_content=file_content_new):
        config = GetConfig(config_filepath=config_filepath)
        if subject_items:
            if subject_items[7]:
                assert (
                    config["Subject"]["date_of_birth"].isoformat() == expected_items[0]
                )
            # Check empty fields have been removed from the config Dict
            assert len(config["Subject"]) == len([x for x in subject_items if x])
        else:
            assert config.get("Subject") is None

        GetConfig.cache_clear()


@pytest.mark.parametrize(
    "filepaths",
    [
        (
            "a/b.xml",
            "./data/v2019_07-2p-Max/serialdata_m8_trim.txt",
            "./data/v2019_07-2p-Max/suite2p_trim",
            "./data/v2019_07-2p-Max/test.nwb",
        ),
        (
            "./data/v2019_07-2p-Max/TSeries-02042020-1448-001_trim.xml",
            "a/b.txt",
            "./data/v2019_07-2p-Max/suite2p_trim",
            "./data/v2019_07-2p-Max/test.nwb",
        ),
        (
            "./data/v2019_07-2p-Max/TSeries-02042020-1448-001_trim.xml",
            "./data/v2019_07-2p-Max/serialdata_m8_trim.txt",
            "my/dir",
            "./data/v2019_07-2p-Max/test.nwb",
        ),
        (
            "./data/v2019_07-2p-Max/TSeries-02042020-1448-001_trim.xml",
            "./data/v2019_07-2p-Max/serialdata_m8_trim.txt",
            "./data/v2019_07-2p-Max/suit2p_trim/Plane_1/spks.npy",
            "./data/v2019_07-2p-Max/test.nwb",
        ),
        (
            "./data/v2019_07-2p-Max/",
            "./data/v2019_07-2p-Max/serialdata_m8_trim.txt",
            "./data/v2019_07-2p-Max/suit2p_trim",
            "./data/v2019_07-2p-Max/test.nwb",
        ),
    ],
)
def test_GetConfig_FileNotFound(filepaths):
    file_content = f"""
[DataInput]
    microscope_filepath_mandatory = "{filepaths[0]}"
    teensy_filepath_mandatory = "{filepaths[1]}"
    suite2pNpy_directory_mandatory = "{filepaths[2]}"
    nwb_output_filepath_mandatory = "{filepaths[3]}"
"""
    config_filepath = Path("test_config.toml")
    with ContextLock(file_name=config_filepath, file_content=file_content):
        with pytest.raises(FileNotFoundError):
            GetConfig(config_filepath=config_filepath)
        GetConfig.cache_clear()


def test_GetConfig_ConfigMissingItem():
    file_content = """
[DataInput]
"""
    config_filepath = Path("test_config.toml")
    with ContextLock(file_name=config_filepath, file_content=file_content):
        with pytest.raises(err.ConfigMissingItem):
            GetConfig(config_filepath=config_filepath)
        GetConfig.cache_clear()


@pytest.mark.parametrize(
    "experiment_version",
    ["lmcecmp", "kjvnkrjnvjonowe", "pcheuieio"],
)
def test_GetConfig_unsupported_experiment_version(experiment_version):
    # this is to satisfy lint
    microscope = "./data/v2019_07-2p-Max/TSeries-02042020-1448-001_trim.xml"
    file_content = f"""
[DataInput]
    experiment_version_mandatory = "f{experiment_version}"
    microscope_filepath_mandatory = "f{microscope}"
    teensy_filepath_mandatory = "./data/v2019_07-2p-Max/serialdata_m8_trim.txt"
    suite2pNpy_directory_mandatory = "./data/v2019_07-2p-Max/suit2p_trim"
    nwb_output_filepath_mandatory = "./data/v2019_07-2p-Max/test.nwb"
"""
    config_filepath = Path("test_config.toml")
    with ContextLock(file_name=config_filepath, file_content=file_content):
        with pytest.raises(err.UnsupportedVersion):
            GetConfig(config_filepath=config_filepath)
        GetConfig.cache_clear()


@pytest.mark.parametrize(
    "test_dict",
    [
        {"lieweip": "", "kunroi5": [1, 2, 3], "iiopv": {"ieeine_mandatory": None}},
        {
            "lieweip": "",
            "kunroi5": [1, 2, 3],
            "iiopv": {"ieeine": {"kvnrnvr_mandatory": ""}},
        },
        {
            "lieweip": "",
            "kunroi5": [1, 2, 3],
            "iiopv": {"ieeine": {"kvnrnvr": {"wemopmopop_mandatory": None}}},
        },
    ],
)
def test_check_mandatory_fields_raised(test_dict):
    with pytest.raises(err.ConfigMissingItem):
        check_mandatory_fields(test_dict)


@given(
    generated_dict=st.recursive(
        base=st.dictionaries(
            keys=st.text(),
            values=st.one_of(
                st.none(), st.floats(), st.integers(), st.lists(elements=st.floats())
            ),
            min_size=1,
            max_size=5,
        ),
        extend=lambda children: st.dictionaries(
            keys=st.text(),
            values=children,
            min_size=1,
            max_size=5,
        ),
        max_leaves=3,
    )
)
def test_check_mandatory_fields(generated_dict):
    assert check_mandatory_fields(generated_dict)


@pytest.mark.parametrize(
    "input_config, missing_fields",
    [
        (
            """
[DataInput]
    experiment_version_mandatory = "v2021.05:2p_imaging_head_fixed+Simon"
    microscope_filepath_mandatory = '''
            ./data/v2021_05-2p-Simon/TSeries-05112021-1455-001_trim.xml'''
    teensy_filepath_mandatory = '''
            ./data/v2021_05-2p-Simon/20210511_Mouse#513_Teensy_serial_data_trim.csv'''
    suite2pNpy_directory_mandatory = "./data/v2021_05-2p-Simon/suite2p_trim"
    nwb_output_filepath_mandatory = "./data/v2021_05-2p-Simon/weeds.nwb"
[NWBFile]
    session_description_mandatory = "Weeds"
[Imaging]
    Device.Type_mandatory = "Microscope"
    PlaneDescription_mandatory = "My awesome imaging plane"
    OpticalChannelDescription_mandatory = "GCaMP/Green, tdTomado/red"
    emission_lambda_mandatory = 520
    CalciumIndicator_mandatory = "jGCaMP7f"
    ImagingLocation_mandatory = "PCX"
           """,
            [
                "NWBFile.lab",
                "NWBFile.institution",
                "NWBFile.timezone",
                "NWBFile.experimenter",
                "NWBFile.experiment_description",
                "Subject.age",
                "Subject.description",
                "Subject.genotype",
                "Subject.sex",
                "Subject.species",
                "Subject.subject_id",
                "Subject.weight",
                "Subject.date_of_birth",
                "Imaging.Device.Description",
                "Imaging.Device.Manufacturer",
                "Imaging.grid_spacing",
                "Imaging.grid_spacing_unit",
                "Imaging.reference_frame",
            ],
        ),
        (
            """
[DataInput]
    experiment_version_mandatory = "v2021.05:2p_imaging_head_fixed+Simon"
    microscope_filepath_mandatory = '''
            ./data/v2021_05-2p-Simon/TSeries-05112021-1455-001_trim.xml'''
    teensy_filepath_mandatory = '''
            ./data/v2021_05-2p-Simon/20210511_Mouse#513_Teensy_serial_data_trim.csv'''
    suite2pNpy_directory_mandatory = "./data/v2021_05-2p-Simon/suite2p_trim"
    nwb_output_filepath_mandatory = "./data/v2021_05-2p-Simon/weeds.nwb"
[NWBFile]
    session_description_mandatory = "Weeds"
[Imaging]
    Device.Type_mandatory = "Microscope"
    PlaneDescription_mandatory = "My awesome imaging plane"
    OpticalChannelDescription_mandatory = "GCaMP/Green, tdTomado/red"
    emission_lambda_mandatory = 520
    CalciumIndicator_mandatory = "jGCaMP7f"
    ImagingLocation_mandatory = "PCX"
    Device.Manufacturer = "Bruker"
    grid_spacing = [2.0, 2.0, 30.0]
[Subject]
    sex = "male"
    species = "mouse"
           """,
            [
                "NWBFile.lab",
                "NWBFile.institution",
                "NWBFile.timezone",
                "NWBFile.experimenter",
                "NWBFile.experiment_description",
                "Subject.age",
                "Subject.description",
                "Subject.genotype",
                "Subject.subject_id",
                "Subject.weight",
                "Subject.date_of_birth",
                "Imaging.Device.Description",
                "Imaging.grid_spacing_unit",
                "Imaging.reference_frame",
            ],
        ),
    ],
)
def test_check_empty_fields(input_config, missing_fields):
    config_filepath = Path("test_config.toml")
    with ContextLock(file_name=config_filepath, file_content=input_config):
        config = GetConfig(config_filepath=config_filepath)
        assert sorted(check_empty_fields(config)) == sorted(missing_fields)

        # Need to clear the cache otherwise the function isn't called
        GetConfig.cache_clear()


@pytest.mark.parametrize(
    "field, path",
    [
        ("microscope_fiLepath_mandatory", TEST_CST.FILEPATHS["v2019_07-2p-Max"]["xml"]),
        ("teensy_filepath_mandatory", TEST_CST.FILEPATHS["v2019_07-2p-Max"]["teensy"]),
        (
            "microscope_Filepath_mandatory",
            TEST_CST.FILEPATHS["v2021_05-2p-Simon"]["xml"],
        ),
        (
            "teensy_filepath_mandatory",
            TEST_CST.FILEPATHS["v2021_05-2p-Simon"]["teensy"],
        ),
        (
            "suite2pNpy_diRECtory_mandatory",
            TEST_CST.FILEPATHS["v2021_05-2p-Simon"]["suite2p"],
        ),
        (
            "microscope_filepath_mandatory",
            TEST_CST.FILEPATHS["v2021_04-2p-Max-spontaneous"]["xml"],
        ),
        ("teensy_filepath_mandatory", TEST_CST.FILEPATHS["v2022_05-2p-Max"]["teensy"]),
        (
            "suite2pNpy_directory_mandatory",
            TEST_CST.FILEPATHS["v2019_07-2p-Max"]["suite2p"],
        ),
    ],
)
def test_check_path_fields(field, path):
    assert check_path_fields(field, path)


@pytest.mark.parametrize(
    "field, path",
    [
        ("microscope_file_mandatory", TEST_CST.FILEPATHS["v2019_07-2p-Max"]["xml"]),
        ("suite2pNpy_DIR", TEST_CST.FILEPATHS["v2021_05-2p-Simon"]["suite2p"]),
    ],
)
def test_check_path_fields_raised_wrong_field(field, path):
    with pytest.raises(ValueError):
        check_path_fields(field, path)


@pytest.mark.parametrize(
    "field, path",
    [
        ("microscope_fiLepath_mandatory", Path("./data/metadata-FILE-NOT-EXIST.xml")),
        ("teensy_filepath_mandatory", Path("./data/v2019_07-2p-Max")),
        ("microscope_filepath_mandatory", Path("./data/Raw-DIRECTORY-NOT-EXIST")),
        (
            "ABC_DIRECTory_optional",
            TEST_CST.FILEPATHS["v2021_04-2p-Max-spontaneous"]["xml"],
        ),
        ("xYz_directORY_test", Path("./data/TSeries-TYPO-NOT-EXIST.env")),
        (
            "suite2pNpy_directory_mandatory",
            TEST_CST.FILEPATHS["v2021_05-2p-Simon"]["suite2p"]
            / "Npy-DIRECTORY-NOT-EXIST",
        ),
    ],
)
def test_check_path_fields_raised_file_error(field, path):
    with pytest.raises(FileNotFoundError):
        check_path_fields(field, path)
