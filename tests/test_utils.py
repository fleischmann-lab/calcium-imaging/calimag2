from pathlib import Path

from filelock import FileLock


class ContextLock:
    """Lock context manager for when tests run in parallel."""

    def __init__(self, file_name, file_content):
        self.filepath = Path(file_name)
        self.lock_path = Path(str(self.filepath) + ".lock")
        self.lock = FileLock(self.lock_path, timeout=1)
        self.lock.acquire(timeout=20)

        with self.filepath.open("w") as fid:
            fid.write(file_content)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.lock.release()
        self.filepath.unlink()
        self.lock_path.unlink()
