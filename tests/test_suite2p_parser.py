import pytest
import test_constants as TEST_CST

from calimag.suite2p_parser import check_number_of_channels


@pytest.mark.parametrize(
    "nchannels_s2p, scope_filepath, expected_nchannels",
    [
        ([1, 1], TEST_CST.FILEPATHS["singles"]["xml_multiplane"], 1),
        ([2, 2], TEST_CST.FILEPATHS["v2021_05-2p-Simon"]["xml"], 2),
    ],
)
def test_check_number_of_channels(nchannels_s2p, scope_filepath, expected_nchannels):
    nchannels = check_number_of_channels(
        nchannels_s2p=nchannels_s2p, scope_filepath=scope_filepath
    )
    assert nchannels == expected_nchannels
