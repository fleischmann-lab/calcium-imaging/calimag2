from pathlib import Path
from typing import Dict

DATA_PATH = Path("data")

FILEPATHS_DICT = {
    "singles": dict(
        teensy="serialdata - 20190710_M002_trim.txt",
        nwb="mock-file.nwb",
        xml_multiplane="metadata - TSeries-07102019-1600-023_trim.xml",
        teensy_utf="serial_mouse#7_trim.txt",
        xml_1plane="TSeries-01122023-0955-003_trim.xml",
    ),
    "v2019_07-2p-Max": dict(
        teensy="serialdata_m8_trim.txt",
        xml="TSeries-02042020-1448-001_trim.xml",
        suite2p="suite2p_trim",
        config="config.toml",
    ),
    "v2021_04-2p-Max-spontaneous": dict(
        teensy="serial data_trim.txt", xml="TSeries-04292021-2139-010_trim.xml"
    ),
    "v2021_05-2p-Simon": dict(
        teensy="20210511_Mouse#513_Teensy_serial_data_trim.csv",
        xml="TSeries-05112021-1455-001_trim.xml",
        suite2p="suite2p_trim",
        config="config.toml",
    ),
    "v2022_05-2p-Max": dict(
        teensy="20220531_ME02_trim.txt", xml="TSeries-05312022-1348-020_trim.xml"
    ),
}

FILEPATHS: Dict = {}

for p, d in FILEPATHS_DICT.items():
    FILEPATHS[p] = {}
    for k, v in d.items():
        FILEPATHS[p][k] = DATA_PATH.joinpath(p, v)

del FILEPATHS_DICT
