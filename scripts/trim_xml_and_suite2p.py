"""Trim XML and Suite2p files."""
import glob
import os
import re
import shutil
import xml.etree.ElementTree as ET

import numpy as np
import pandas as pd
from tqdm.notebook import tqdm


def trim_suite2p_plane(
    plane_directory,
    new_nframes,
    old_directory,
    new_directory,
    remove_opsfields=["regPC"],
):
    """Trim single plane of Suite2p file."""
    # TODO: handle more channels

    old_plane = os.path.join(old_directory, plane_directory)
    new_plane = os.path.join(new_directory, plane_directory)

    if not os.path.exists(new_plane):
        os.mkdir(new_plane)

    # only copy
    copy_only = ["iscell.npy", "stat.npy"]
    for f in copy_only:
        shutil.copyfile(
            os.path.join(old_plane, f),
            os.path.join(new_plane, f),
        )

    # ops
    ops_file = os.path.join(old_plane, "ops.npy")
    ops = np.load(ops_file, allow_pickle=True).item()

    # TODO: remove this, this is just to show how big that variable is
    # np.save(
    #     os.path.join(new_plane, 'ops-regPC.npy'),
    #     ops['regPC'],
    #     allow_pickle=True
    # )

    # frame number
    old_nframes = ops["nframes"]
    ops["nframes"] = new_nframes

    # regPC + file related ops -> turn empty
    for k in remove_opsfields:
        if k in ops:
            ops[k] = ["removed when trimming"]

    # registration related ops? -> slice 1st dim
    for k, v in ops.items():
        if re.search("^(xoff|yoff|corrXY|badframes)", k) is None:
            continue
        assert v.shape[0] == old_nframes
        v = v[:new_nframes]
        assert v.shape[0] == new_nframes
        ops[k] = v

    np.save(os.path.join(new_plane, "ops.npy"), ops, allow_pickle=True)

    # slice array -> slice 2nd dim
    slice_mat = [
        "F.npy",
        "Fneu.npy",
        "spks.npy",
        "F_chan2.npy",
        "Fneu_chan2.npy",
        "spks_chan2.npy",
    ]
    for f in slice_mat:
        f_path = os.path.join(old_plane, f)
        if not os.path.exists(f_path):
            continue
        d = np.load(f_path)
        assert d.shape[1] == old_nframes
        d = d[:, :new_nframes]
        assert d.shape[1] == new_nframes
        np.save(os.path.join(new_plane, f), d)


def trim_xml(
    xml_file, one_plane_30Hz=False, max_trial=10, suffix="trim", overwrite=False
):
    """Trim XML file."""
    out_file = xml_file.replace(".xml", f"_{suffix}.xml")
    if xml_file.endswith(f"_{suffix}.xml"):
        return

    if os.path.exists(out_file) and not overwrite:
        print(
            f'The file "{out_file}" already exists. '
            "Skipping. Use `overwrite=True` if want to overwrite"
        )
        return

    root = ET.parse(xml_file).getroot()

    xml_type = get_xml_type(root)
    stop_idx = None
    if xml_type == "ZSERIES":  # Multiplane XML files
        # get sequence times and cycles
        sequences = root.findall("./Sequence")

        df = []
        for seq in sequences:
            cycle = seq.get("cycle")
            reltime = [
                frame.get("relativeTime")
                for frame in seq.findall("./Frame[@relativeTime]")
            ]

            assert all([x is not None for x in reltime + [cycle]])

            df.append(dict(relative=min([float(x) for x in reltime]), cycle=int(cycle)))

        df = pd.DataFrame(df)
        assert df.cycle.is_monotonic_increasing

        # get boundaries between trials
        resets = np.where(df.relative.diff() < 0)[0]
        start_idx = np.concatenate([[0], resets])
        stop_idx = np.concatenate([resets - 1, [len(df) - 1]])

        # remove trials above max_trial; currently slow but not too terrible
        cycles_to_remove = df.iloc[start_idx[max_trial] :].cycle.to_list()

        for cycle in tqdm(cycles_to_remove, leave=False, desc="Removing cycles"):
            seq2rm = root.findall(f"./Sequence[@cycle='{cycle:d}']")
            assert len(seq2rm) == 1
            root.remove(seq2rm[0])

    elif xml_type == "TIMED":  # Max acquisition frequency - only one plane
        trials = root.findall("./Sequence/Frame[@relativeTime='0']..")
        trials_to_remove = trials[max_trial:]
        for trial in trials_to_remove:
            root.remove(trial)

    # write to file
    with open(out_file, "wb") as f:
        ET.ElementTree(root).write(f)

    dirname = os.path.dirname(xml_file)
    return stop_idx, dirname


def trim_suite2p(
    dirname,
    stop_idx,
    max_trial=10,
    suffix="trim",
    remove_opsfields=["regPC"],
):
    """Trim suite2p files."""
    suite2p_dir = os.path.join(dirname, "suite2p")
    if not os.path.exists(suite2p_dir):
        print(f'No "{suite2p_dir}" found. Not trimming suite2p.')
        return
    trim_suite2p_dir = os.path.join(dirname, f"suite2p_{suffix}")

    if not os.path.exists(trim_suite2p_dir):
        os.mkdir(trim_suite2p_dir)

    plane_folders = [x for x in os.listdir(suite2p_dir) if "plane" in x.lower()]

    new_nframes = stop_idx[max_trial - 1] + 1  # from XML trimming above

    for p in tqdm(plane_folders, leave=False, desc="Suite2p plane trimming"):
        trim_suite2p_plane(
            plane_directory=p,
            new_nframes=new_nframes,
            old_directory=suite2p_dir,
            new_directory=trim_suite2p_dir,
            remove_opsfields=remove_opsfields,
        )


def get_xml_type(root_xml) -> str:
    """Get the the type of the XML file.

    Types can be:
    - "ZSeries": multiplane XML files
    - "Timed": only one plane 30Hz (max acquisition frequency) XML file
    """
    type_elems_xml = root_xml.find("./Sequence")
    type_elems = type_elems_xml.get("type")
    assert isinstance(type_elems, str)
    xml_type = type_elems.split(" ")[1]
    return xml_type


def trim_xml_and_suite2p(
    xml_file, max_trial=10, suffix="trim", overwrite=False, remove_opsfields=["regPC"]
):
    """Trim both Suite2p and XML files."""
    stop_idx, dirname = trim_xml(
        xml_file,
        max_trial=10,
        suffix="trim",
        overwrite=False,
    )
    trim_suite2p(
        dirname,
        stop_idx,
        max_trial=10,
        suffix="trim",
        remove_opsfields=["regPC"],
    )


if __name__ == "__main__":
    xml_files = glob.glob("v20*/*.xml")
    print(xml_files)

    ops_fields_to_remove = [
        "Vmap",
        "tPC",
        "regPC",
        "first_tiffs",
        "filelist",
        "frames_per_file",
    ]
    for xml_file in tqdm(xml_files, desc="XML + Suite2p trimming"):
        trim_xml_and_suite2p(
            xml_file, overwrite=True, remove_opsfields=ops_fields_to_remove
        )
