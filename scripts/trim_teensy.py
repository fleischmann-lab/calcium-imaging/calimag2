"""Trim Teensy files."""
from pathlib import Path

from chardet.universaldetector import UniversalDetector

file_in = Path("./data/v2021_04-2p-Max-spontaneous/serial data.txt")
file_out = Path("./data/v2021_04-2p-Max-spontaneous/serial data_trim.txt")
# file_in = Path("./data/v2022_05-2p-Max/20220531_ME02.txt")
# file_out = Path("./data/v2022_05-2p-Max/20220531_ME02_trim.txt")


def detect_encoding(filepath):
    """Detect encoding of the file."""
    with open(filepath, "rb") as fid:
        detector = UniversalDetector()
        for line in fid.readlines():
            detector.feed(line)
            if detector.done:
                break
        detector.close()
    return detector.result.get("encoding")


def trim_teensy(file_in, file_out, trials=10):
    """Trim Teensy file."""
    print(f"Input: {file_in}")
    print(f"Output: {file_out}")

    encoding = detect_encoding(filepath=file_in)

    with open(file_in, encoding=encoding) as fid:
        rows = fid.readlines()

    if "v2022_05-2p-Max".lower() in str(file_in).lower():
        for idx, row in enumerate(rows):
            data = row.split(",")
            if len(data) == 6:
                if int(data[5].strip()) == trials + 1:
                    select_idx = idx
                    break

        # Select the first trials
        select_rows = rows[0:select_idx]

    elif "spontaneous" in str(file_in).lower():
        # Spontaneous so no trial, let's just arbitrarily select the first "chunk"
        trials_idx = []
        for idx, row in enumerate(rows):
            data = row.split(",")
            if len(data) == 3:
                if int(data[0].strip()) == 30000:
                    trials_idx.append(idx)
        select_rows = rows[0 : trials_idx[trials - 1] + 1]

    else:
        select_idx = []
        for idx, row in enumerate(rows):
            if "waiting for the next odor" in row:
                select_idx.append(idx)

        # Select the first trials
        select_rows = rows[0 : select_idx[trials - 1] + 1]

    with open(file_out, "w") as fid:
        fid.writelines(select_rows)


if __name__ == "__main__":
    trim_teensy(file_in, file_out, trials=10)
    print("Done !")
