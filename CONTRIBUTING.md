# Contributing

## Development

### Setup

To set up a development installation, first install the HDF5 binaries. The right HDF5 version to install is specified in the [`environment.yml`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag2/-/blob/master/environment.yml) file. Then clone the repository and install the development dependencies:

```shell
git clone https://gitlab.com/fleischmann-lab/calcium-imaging/calimag2.git
cd calimag2
python -m venv .venv
source .venv/bin/activate
pip install -e .[dev]
```

To obtain the data and use `datalad` (already included inside `pyproject.toml` development dependencies, also install `git-annex`. Follow [`datalad` suggestions](https://handbook.datalad.org/en/latest/intro/installation.html)

- [ ] Complete section on datalad
- For the time being please refer to [this](https://gitlab.com/tuanhpham/tst-gl-gin-dtl)

The datalad GIN remote is <https://gin.g-node.org/fleischmann-lab/calimag2>

### Testing

The CI uses [GNU Make](https://www.gnu.org/software/make/) which can be installed by running the following commands:

- Linux: `sudo apt install make` (depends on the distribution/package manager)
- MacOS: `brew install make`, the command should then be available as `gmake` instead of `make` (https://formulae.brew.sh/formula/make)
- Windows: `choco install make` (https://community.chocolatey.org/packages/make)

Check that all the tests are passing by running `make test`. If everything is green ✔, your development installation should be ready!

### Linting/Formatting

You can autoformat some files by running `make format` (need `prettier` from `npm` if using `make prettier-format`), and you can check for the linting CI job before pushing by running `make lint-python` or `make lint-all`.

### Precommit hooks

To activate the pre-commit hook, while `.venv` is activated, you can run `pre-commit install` once. As of now, the hook is only for lint checks. For commits that you do not want the hook to take effect, do `git commit --no-verify`.

## Release

The `calimag2` Conda package is automatically pushed to the [FleischmannLab channel on Anaconda.org](https://anaconda.org/FleischmannLab/calimag2) when a new Git tag is pushed to the `master` branch. The list of all tags is available [here](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag2/-/tags).
For pre-releases on a development branch (i.e. release candidates with tag ending in `rc[X]`), the `windows` job needs to be triggered manually for the release job to be activated.

Gitlab CI needs to know the [Anaconda API token](https://anaconda.org/FleischmannLab/settings/access) to have the rights to push the newly built Conda package to [Anaconda.org](https://anaconda.org/). This token needs to be rotated (for now every year) and then copied as a [CI/CD variable to Gitlab](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag2/-/settings/ci_cd).

To build the package locally, you need to have [Mamba installed](https://mamba.readthedocs.io/en/latest/installation.html). Then run `conda mambabuild --channel conda-forge recipe/`. The newly built package will be located in your `<path/to/(mini)conda-directory>/conda-bld/` directory.
