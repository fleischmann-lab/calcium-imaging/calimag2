# Calimag

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/fleischmann-lab/calcium-imaging/calimag2?branch=master&label=pipeline&style=flat-square)](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag2/-/commits/master)
[![coverage report](https://img.shields.io/gitlab/pipeline-coverage/fleischmann-lab/calcium-imaging/calimag2?branch=master&job_name=test&style=flat-square)](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag2/-/commits/master)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

[![conda package](https://img.shields.io/conda/v/fleischmannlab/calimag?color=blue&style=flat-square)](https://anaconda.org/FleischmannLab/calimag)
[![license](https://img.shields.io/gitlab/license/fleischmann-lab/calcium-imaging/calimag2?color=yellow&label=license&style=flat-square)](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag2/-/blob/master/LICENSE)

CLI tool to convert behavior & calcium imaging data from the Fleischmann's lab (Brown University) to the NWB standard file format.

`calimag` is currently migrating from [`calimag`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag).

The GIN remote is at <https://gin.g-node.org/fleischmann-lab/calimag2>.

## MIGRATION TODOs

- [ ] Double check for naming `calimag` to replace with `calimag2` where appropriate
- [x] See if can use `calimag` instead of `calimag2` for package import:
  - [ ] Unclear of potential side effects, e.g. in `conda` or installations of both `calimag` and `calimag2`
- [ ] See if possible to co-author and sign with GIN username
- [ ] Unclear how to set `.gitattributes` properly to force `datalad save/status` to ignore most folders except `data`
- [ ] When pushing to GIN, `datalad push` doesn't push tags. Unclear if side effects when doing `git push --tags` afterwards
- [ ] Unclear if [this](https://brainhack-princeton.github.io/handbook/content_pages/03-06-sampleProjectWithDatalad.html) i a better approach: no-annex first then datalad for subdataset (I think that's what they did?)

## Table of Contents

<!-- - [Background](#background) -->

- [Overview](#overview)
- [Install](#install)
- [Usage](#usage)
- [Development](#development)

<!-- ## Background -->

## Overview

Here's a video demonstrating how to use the tool from the command line:

![Demo video](docs/demo.ogv)

The resulting NWB file will look like the following:

![NWB output](docs/nwb-output.png)

You can use [HDFView](https://www.hdfgroup.org/downloads/hdfview/) to easily browse inside the NWB file. The NWB file produced should also be easily shareable on [DANDI](https://dandiarchive.org/) without modifications.

## Setup

### Prerequisites

You need to have the `Conda` package manager installed. You can get it from the [Anaconda](https://www.anaconda.com/products/individual) or the [Miniconda](https://docs.conda.io/en/latest/miniconda.html) distributions.

### Install

To install the package, you can optionally create a new isolated environment for it and activate this environment:

```
conda create -n calimag python=3.10
conda activate calimag
```

Then to install it, run the following commands:

```
conda install -c conda-forge -c fleischmannlab calimag
```

### Update

To update the package to the latest version, first activate the right environment, for example:

```
conda activate calimag
```

Then run the following command:

```
conda update calimag
```

The different versions are listed [here](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag2/-/tags).

## Usage

### Configuration

Download [this configuration file](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag2/-/blob/master/tests/user.config.toml) to your local machine and replace the file paths and the metadata by your own values.

### Experiment version

The allowed experiment versions labels are defined in the [`constants.py` file at the `EXPERIMENT_VERSIONS` key](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag2/-/blob/master/src/calimag/constants.py).

### Convert your files to NWB

Run the following command in your terminal:

```bash
calimag "/path/to/your/config/file.toml"
```
