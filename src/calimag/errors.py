"""Custom errors classes."""


class TimestampsStartBeforeFileStartTime(Exception):
    """If first timestamps start before the file start time."""

    pass


class SessionStartTimeError(Exception):
    """Missing session start time."""

    pass


class UnsupportedVersion(Exception):
    """Unsupported data file version."""

    pass


class XmlElementNotFound(Exception):
    """Exception signaling missing elements in the XML file."""

    pass


class ConfigMissingItem(Exception):
    """Exception to signal mandatory missing items in the config file."""

    pass
