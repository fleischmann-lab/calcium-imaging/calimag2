"""Command Line Interface to run the software."""
from pathlib import Path

import click

import calimag as calimag
from calimag.config import GetConfig, check_empty_fields
from calimag.nwb_converter import NwbConverter


@click.command()
@click.argument("configfile", type=click.Path(exists=True))
def cli(configfile):
    """Convert data to NWB file based on your CONFIGFILE."""
    click.echo(f"\nCalimag version {calimag.__version__}")
    click.echo(
        "\nUsing the following configuration file:\n"
        f"{click.format_filename(str(Path(configfile).absolute()))}"
    )
    config = GetConfig(configfile)

    empty_fields = check_empty_fields(config)
    if empty_fields:
        click.echo(f"\nThe following fields in the config are empty: {empty_fields}")

        # Ask the user to continue or not
        user_choice = input("\nContinue anyway? [y/n] ")
        if user_choice.lower() != "y":
            click.echo("Aborted!")
            return

    DataInput = config.get("DataInput")
    experiment_version = DataInput.get("experiment_version_mandatory")
    click.echo(f"\nExperiment version: {experiment_version}")
    nwb_test_path = Path(DataInput.get("nwb_output_filepath_mandatory"))
    click.echo(
        "\nThe resulting NWB file will be written at the following location:\n"
        f"{click.format_filename(str(nwb_test_path.absolute()))}"
    )

    # Ask the user to continue or not
    user_choice = input("\nContinue? [y/n] ")
    if user_choice.lower() == "y":
        # Run Calimag
        conv = NwbConverter(config_filepath=configfile)
        conv.Convert2NWB()
    else:
        click.echo("Aborted!")
    return


if __name__ == "__main__":
    cli()
