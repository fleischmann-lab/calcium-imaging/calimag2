"""List of default constants."""
from typing import List

# Version pattern:
# vYYYY.MM:type-of-experiment[+optional-experimenter-name][:optional-label]
EXPERIMENT_VERSIONS: List[str] = [
    "v2021.05:2p_imaging_head_fixed+Simon",
    "v2019.07:2p_imaging_head_fixed+Max",
    "v2021.04:2p_imaging_head_fixed+Max:spontaneous-activity",
    "v2022.05:2p_imaging_head_fixed+Max",
    "v2020.06:2p_imaging_head_fixed+Simon",
]
TEENSY_HEADER = {
    EXPERIMENT_VERSIONS[0]: [
        {
            "name": "time",
            "unit": "ms",
            "SI_unit": "sec",
            "conversion_factor": 1,  # TODO: Temporary hack, should be 1e-3
        },
        {"name": "odor", "unit": "-", "SI_unit": "-", "conversion_factor": 1},
        {
            "name": "unknown1",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
        },
        {
            "name": "wheel",
            "unit": "rad",
            "SI_unit": "rad",
            "conversion_factor": 1,
        },
        {"name": "trial", "unit": "-", "SI_unit": "-", "conversion_factor": 1},
        {
            "name": "unknown2",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
        },
    ],
    EXPERIMENT_VERSIONS[1]: [
        {
            "name": "time",
            "unit": "ms",
            "SI_unit": "sec",
            "conversion_factor": 1,  # TODO: Temporary hack, should be 1e-3
        },
        {
            "name": "odor",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
        },
        {
            "name": "flow",
            "unit": "cm3/min",
            "SI_unit": "m3/sec",
            "conversion_factor": 60 * 1e-6,
        },
        {
            "name": "wheel",
            "unit": "rad",
            "SI_unit": "rad",
            "conversion_factor": 1,
        },
        {
            "name": "trial",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
        },
    ],
    EXPERIMENT_VERSIONS[2]: [
        {
            "name": "time",
            "unit": "ms",
            "SI_unit": "sec",
            "conversion_factor": 1,  # TODO: Temporary hack, should be 1e-3
        },
        {
            "name": "flow",
            "unit": "cm3/min",
            "SI_unit": "m3/sec",
            "conversion_factor": 60 * 1e-6,
        },
        {
            "name": "wheel",
            "unit": "rad",
            "SI_unit": "rad",
            "conversion_factor": 1,
        },
    ],
    EXPERIMENT_VERSIONS[3]: [
        {
            "name": "time",
            "unit": "ms",
            "SI_unit": "sec",
            "conversion_factor": 1,  # TODO: Temporary hack, should be 1e-3
        },
        {"name": "odor", "unit": "-", "SI_unit": "-", "conversion_factor": 1},
        {
            "name": "flow",
            "unit": "cm3/min",
            "SI_unit": "m3/sec",
            "conversion_factor": 60 * 1e-6,
        },
        {
            "name": "wheel",
            "unit": "rad",
            "SI_unit": "rad",
            "conversion_factor": 1,
        },
        {
            "name": "odor_on",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
        },
        {"name": "trial", "unit": "-", "SI_unit": "-", "conversion_factor": 1},
    ],
    EXPERIMENT_VERSIONS[4]: [
        {
            "name": "time",
            "unit": "ms",
            "SI_unit": "sec",
            "conversion_factor": 1,  # TODO: Temporary hack, should be 1e-3
        },
        {
            "name": "odor",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
        },
        {
            "name": "unknown1",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
        },
        {
            "name": "wheel",
            "unit": "rad",
            "SI_unit": "rad",
            "conversion_factor": 1,
        },
        {
            "name": "trial",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
        },
        {
            "name": "unknown2",
            "unit": "-",
            "SI_unit": "-",
            "conversion_factor": 1,
        },
    ],
}

TIMEZONE: str = "EST"
WHEEL_MAX_ENCODING: int = 4096
# WHEEL_MAX_RATE = 100


CONFIG_REFERENCE = {
    "DataInput": [
        "experiment_version_mandatory",
        "microscope_filepath_mandatory",
        "teensy_filepath_mandatory",
        "suite2pNpy_directory_mandatory",
        "nwb_output_filepath_mandatory",
    ],
    "NWBFile": [
        "session_description_mandatory",
        "lab",
        "institution",
        "timezone",
        "experimenter",
        "experiment_description",
    ],
    "Subject": [
        "age",
        "description",
        "genotype",
        "sex",
        "species",
        "subject_id",
        "weight",
        "date_of_birth",
    ],
    "Imaging": [
        {"Device": ["Type_mandatory", "Description", "Manufacturer"]},
        "OpticalChannelDescription_mandatory",
        "emission_lambda_mandatory",
        "PlaneDescription_mandatory",
        "CalciumIndicator_mandatory",
        "ImagingLocation_mandatory",
        "grid_spacing",
        "grid_spacing_unit",
        "reference_frame",
    ],
}

MICROSCOPE_SUPPORTED_VERSIONS = {"5.4.64.700"}
SUPPORTED_XML_TYPES = {"ZSERIES", "TIMED"}
