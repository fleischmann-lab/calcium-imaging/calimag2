##
# Calimag
#
# @file
# @version 0.1

help: ## Show this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

add-gin-ssh: ## Add GIN remote via SSH
	datalad siblings add -d . --name gin --url git@gin.g-node.org:/fleischmann-lab/calimag2.git

add-gin-https: ## Add GIN remote via HTTPS
	datalad siblings add -d . --name gin --url https://gin.g-node.org/fleischmann-lab/calimag2
	git config remote.gin.annex-ignore false

ci-install-git-annex: ## Install git-annex on CI
	# Source: https://neuro.debian.net/install_pkg.html?p=git-annex-standalone
	# The base image of python:3.10-slim is debian:bullseye-slim
	apt-get install -y wget gnupg2
	wget -O- http://neuro.debian.net/lists/bullseye.us-nh.libre | tee /etc/apt/sources.list.d/neurodebian.sources.list
	apt-key adv --recv-keys --keyserver hkps://keyserver.ubuntu.com 0xA5D32F012649A5A9
	apt-get update
	apt-get install -y git-annex-standalone
	git-annex version

get-data: # Update and obtain the whole data folder
	datalad update
	datalad get --jobs 2 data

ci-get-data: ## Obtain data on CI
	apt-get install -y datalad # unclear if conflict with pyproject.toml
	datalad --version
	git-annex version
	# make ci-install-git-annex
	make add-gin-https
	make get-data
	du -hL data  # failed if broken symlink

black-check:
	black --check .

black-format:
	black .

isort-check:
	isort --check .

isort-format:
	isort .

flake8:
	flake8 .

pydocstyle:
	pydocstyle .

mypy:
	mypy -p calimag --lineprecision-report .
	cat lineprecision.txt

codespell:
	codespell

prettier-check:
	prettier --check "**/*.{yaml,yml}"
	prettier --check "**/*.md"

prettier-format:
	prettier --write "**/*.{yaml,yml}"
	prettier --write "**/*.md"

format: ## Autoformat everything
	make isort-format
	make black-format
	make prettier-format

lint-python: ## Run Python based static analysis
	make flake8
	make isort-check
	make pydocstyle
	make black-check
	make mypy
	make codespell

lint-all: ## Run all static analysis
	make lint-python
	make prettier-check

test: ## Run tests
	pytest --cov=calimag -n auto tests/ --durations=10 -v

test-xml: ## Run tests with XML coverage
	pytest --cov=calimag --cov-append tests/ --durations=10 -v
	coverage report
	coverage xml

test-html: ## Run tests with HTML report
	pytest --cov=calimag --cov-report html -n auto tests/ --durations=10 -v

clean: ## Clean tmp files
	find . -type f -name *.pyc -delete
	find . -type d -name __pycache__ -delete

guix-shell: ## Spawn a isolated shell
	guix shell -m manifest.scm --pure

.ONESHELL:
conda-pkg: ## Build Conda package
	conda mambabuild --token $$ANACONDA_TOKEN --user $$ANACONDA_USER --channel conda-forge recipe/

# end
